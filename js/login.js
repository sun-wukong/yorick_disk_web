document.write("<script src='js/httpUtil.js'></script>");
document.write("<script src='js/common.js'></script>");

function getCode(){
    var jsonObj = {};
    var result=httpPost(baseUrl+"/login/getCode",jsonObj);
    if(result.flag==1) {
        drawPic(result.result);
    }
}
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    // 兼容FF和IE和Opera
    var theEvent = e || window.event;
    var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    // 13 代表 回车键
    if (code == 13) {
        doLogin();
        return false;
    }
    return true;
}

$("#loginBtn").click(function () {
    doLogin();
})

function doLogin(){
    var username = $("#username").val();
    var password = $("#password").val();
    var code = $("#code").val();
    var jsonObj = {"account":username,"password":password,"code":code};
    var result=httpPost(baseUrl+"/login/doLogin",jsonObj);
    if(result.flag==1){
        storeUserMsg(result.result)
        location.href="index.html"
    }else{
        toastr.error("请检查后重试，或点击忘记密码",result.msg);
    }

}

function storeUserMsg(result){
    ls.setItem("username",result.username)
    ls.setItem("aid",result.aid)
    ls.setItem("account",result.account)
    ls.setItem("mobilephone",result.mobilephone)
    ls.setItem("email",result.email)
    ls.setItem("head",result.head)
    ls.setItem("superRole",result.superRole)
    ls.setItem("department",JSON.stringify(result.department))
    ls.setItem("lastTime",result.lastTime)
    ls.setItem("menus",JSON.stringify(result.menus))
}