var ls = window.localStorage;

function getQueryString(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)return  unescape(r[2]); return null;
}

// 字符串转base64
function encode(str){
    // 对字符串进行编码
    var encode = encodeURI(str);
    // 对编码的字符串转化base64
    var base64 = btoa(encode);
    return base64;
}
// base64转字符串
function decode(base64){
    // 对base64转编码
    var decode = atob(base64);
    // 编码转字符串
    var str = decodeURI(decode);
    return str;
}

var colorList=new Array("bg-primary","bg-secondary","bg-orange","bg-cyan","bg-dark","bg-danger","bg-pink");

function yorickAssert(val, errorMsg, errorCode){
    if(!val){
        toastr.error(errorMsg,errorCode);
        throw errorMsg;
    }
}

function getSelectValue(id) {
    var obj = document.getElementById(id); //定位id

    var index = obj.selectedIndex; // 选中索引

    var value = obj.options[index].value; // 选中值

    return value;
}

function getCheckValue(name) {
    var obj = document.getElementsByName(name);
    var resultList = new Array();
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].checked){
            resultList.push(obj[i].value)
        }
    }
    return resultList
}

function enabledRadio(name, enabled){
    var radio = document.getElementsByName(name);
    if (enabled == 1) {
        radio[0].checked = true;
    } else {
        radio[1].checked = true;
    }

    //Flat purple color scheme for iCheck
    $('input[type="checkbox"].flat-purple, input[type="radio"].flat-purple').iCheck({
        checkboxClass: 'icheckbox_flat-purple',
        radioClass: 'iradio_flat-purple'
    })
}

$("body").on("click", ".removeclass", function (e) {
    var card =  $(this).parent('div').parent("div");
    card.removeClass("zoomIn")
    card.addClass("zoomOut")
    setTimeout(function () {
       card.hide("",function () {
           card.remove()
       })
    },200)

    return false;
})


String.prototype.format = function() {
    if (arguments.length == 0) return this;
    var param = arguments[0];
    var s = this;
    if (typeof (param) == 'object') {
        for (var key in param)
            s = s.replace(new RegExp("\\{" + key + "\\}", "g"), param[key]);
        return s;
    } else {
        for (var i = 0; i < arguments.length; i++)
            s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
        return s;
    }
}

function removeElementAndSiblings(parentId) {
    var targetElement = document.getElementById(parentId)

    if (targetElement) {
        var parentElement = targetElement.parentNode;
        var siblingElements = Array.from(parentElement.childNodes);

        var startIndex = siblingElements.indexOf(targetElement);
        if (startIndex !== -1) {
            for (var i = startIndex; i < siblingElements.length; i++) {
                parentElement.removeChild(siblingElements[i]);
            }
        }
    }
}

async function getFileHash(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = async (event) => {
            const arrayBuffer = event.target.result;
            try {
                const hashBuffer = await crypto.subtle.digest('SHA-256', arrayBuffer);
                const hashArray = Array.from(new Uint8Array(hashBuffer));
                const hashHex = hashArray.map(byte => byte.toString(16).padStart(2, '0')).join('');
                resolve(hashHex);
            } catch (error) {
                reject(error);
            }
        };
        reader.onerror = (event) => {
            reject(new Error('Error reading file'));
        };
        reader.readAsArrayBuffer(file);
    });
}