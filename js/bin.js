document.write("<script src='js/file.js'></script>");

var treeFile;

function getTreeData(){
    var result = httpPost(baseUrl+"/bin/fileList", null);
    if(result.flag==1){
        treeFile = result.result
        drawBinFile();
    }else{
        toastr.error("错误",result.msg);
    }
    $(".app-content").bind('contextmenu', function (e) {
        e.preventDefault(); // prevents native menu from being shown
    });
    document.addEventListener("click", function () {
        $("#popupRC").css({display: "none" });
    });
}
function drawBinFile(){
    console.log(treeFile)
    var fileList = $('#fileList');
    fileList.empty();
        for (var i=0;i<treeFile.length;i++){
            if(treeFile[i].fileType==0){
                fileList.append("<a class=\"btn btn-app mt-2 btn-file\" title='文件名："+treeFile[i].fileName+"' onclick='childFile("+i+")'>" +
                    fileIcon(treeFile[i].fileType) +
                    "<span hidden='hidden'>"+treeFile[i].id+"</span>"+
                    treeFile[i].fileName+"</a>\n")
            }else{
                fileList.append("<a class=\"btn btn-app mt-2 btn-file\" title='文件名："+treeFile[i].fileName+"' onclick='binFileDetail()'>" +
                    fileIcon(treeFile[i].fileType) +
                    "<span hidden='hidden'>"+treeFile[i].id+"</span>"+
                    treeFile[i].fileName+"</a>\n")
            }

        }
    popupRC();
}

function childFile(idx){
    treeFile = treeFile[idx].children
    drawBinFile()
}

function binFileDetail(){
    $("#binFileModal").modal('show')
}

function restoreFile(){
    var jsonObj = {"fileId": rightClickId};
    var result=httpPost(baseUrl+"/bin/restoreFile",jsonObj);
    if(result.flag==1){
        getTreeData()
    } else {
        toastr.error("错误",result.msg);
    }
}


function deleteForeverModal(){
    $("#deleteForeverModal").modal('show');
}


$("#deleteForeverBtn").click(function () {
    var jsonObj = {"fileId": rightClickId};
    var result=httpPost(baseUrl+"/bin/deleteFile",jsonObj);
    if(result.flag==1){
        $("#deleteForeverModal").modal('hide');
        getTreeData()
    } else {
        toastr.error("错误",result.msg);
    }
})