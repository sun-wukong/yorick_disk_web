document.write("<script src='js/httpUtil.js'></script>");
document.write("<script src='js/common.js'></script>");
function register(){
    var account = $("#account").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var jsonObj =
        {
            "account": account,
            "password": password,
            "username": username,
            "mobilephone": phone,
            "email": email
        }
    var result=httpPost(baseUrl+"/register/doRegister",jsonObj);
    if(result.flag==1) {
        toastr.success("账号注册成功", "成功");
        setTimeout(function() {
           location.href="login.html";
        }, 2000);
    }else{
        toastr.error("系统错误", result.msg);
    }
}